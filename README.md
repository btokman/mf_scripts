[![Maifire](https://avatars2.githubusercontent.com/u/16989772?s=200&v=4)](https://github.com/mailfire)

# Mailfire scripts

- - - - 

### Install

Clone project
```sh
  $ git clone <repoUrl>
```
Install dependencies

```sh
  $ npm install
```
Create new buid
```sh
  $ BUILD=<buildNumber> npm run build
```
### Examples
##
#### Include form with subscribtion (email) on your site:
##
##
```js
    (function (w, d, f) {
       w[f] = function () {(w[f].q = w[f].q || []).push(arguments)};
       w['_mf_object_name'] = f;
       var l = document.createElement('script');
       l.src = <pathToLatestBuild>;
       l.type = 'text/javascript';
       l.async = true;
       var s = d.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(l, s);
  })(window, document, 'mf');

  mf.project = <projectId>; // Your project ID
  mf('email', 'init');
```

#### Include webpush (via FCN) on your website:
##
##
```js
    (function (w, d, f) {
       w[f] = function () {(w[f].q = w[f].q || []).push(arguments)};
       w['_mf_object_name'] = f;
       var l = document.createElement('script');
       l.src = <pathToLatestBuild>;
       l.type = 'text/javascript';
       l.async = true;
       var s = d.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(l, s);
  })(window, document, 'mf');

  mf.project = <projectId>; // Your project ID
  mf.msgSenderId = <messageSenderId>; // required for webpush messages
  mf.sw = <pathToYourServiceWorkerFile>; // service worker URL | change for your own

  mf('webpushfcm', 'init');
```
### Call events from iframe
##
#### Close popup
##
##
```html
  <div ....
  onclick="window.parent.postMessage(JSON.stringify({  action : 'close' }), document.referrer)"></div>
```
##
#### Ask webpush notification permission
##
##
```html
  <div ....
  onclick="window.parent.postMessage(JSON.stringify({  action : 'subscribe' }),document.referrer)"></div>
```
##
### TODO
* Migrate to Webpuck 4
* Implementation webpush by gcm
* Migrate to tyoeScript
