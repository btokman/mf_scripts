import 'babel-polyfill';
import Session from './session';
import WebPushFcm from './webpush/fcm';
import postMessageHandler from './helpers/_reciver';
import Email from './email';
import Cookie from './cookie';

/**
 * Register postMessage (handle messages from iframe)
 */
if (window.addEventListener) {
  window.addEventListener('message', postMessageHandler, false);
} else {
  window.attachEvent('onmessage', postMessageHandler);
}x

const bootstrap = async () => {
  /** Get mailFire object */
  const mfObject = window[window._mf_object_name];

  /** Init session */
  const session = new Session(mfObject.project, mfObject.msgSenderId, mfObject.sw, mfObject.api);

  /**  Wait while settings loading */
  await session.loadSettings();

  /** Init application */
  for (const a in mfObject.q) {
    if (mfObject.q.hasOwnProperty(a)) {
      switch (mfObject.q[a][0]) {
        case 'webpushfcm':
          if (Notification.permission !== 'default' || Cookie.get('pushDeny')) return;
          (new WebPushFcm()).init();
          window.mfPopup.showPopup(1000);
          break;
        case 'email':
          if (Cookie.get('subscribed') || window.mfSession.userId || Cookie.get('emailDeny')) return;
          (new Email()).init();
          window.mfPopup.showPopup(1000);
          break;
        default:
          break;
      }
    }
  }
};

bootstrap();