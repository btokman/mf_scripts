import jss from 'jss';
import preset from 'jss-preset-default';


export default class Popup {
  /**
   * Close callback function
   * @type {boolean}
   */
  closeCallback = false;

  /**
   * Subscribe callback function
   * @type {boolean}
   */
  subscribeEvent = false;

  /**
   * Popup wrapper styles
   */
  style = {
    mfBg: {
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      zIndex: 99999999,
      opacity: 0,
      position: 'fixed',
      background: 'rgba(0,0,0,0.7)',
      transition: 'opacity 1s ease',
    },
    mfWrapper: {
      width: `${this.width}px`,
      height: `${this.height}px`,
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: 'auto',
      position: 'absolute',
      minWidth: '320px',
    },
    mfIframe: {
      width: '100%',
      height: '100%',
      border: 0,
    },
  };

  constructor(url, width = '100', height = '245px') {
    this.body = document.body;
    this.url = url;
    this.width = `${width}px`;
    this.height = `${height}px`;

    /** Init css in js */
    jss.setup(preset());
    this.styleObj = jss.createStyleSheet(this.style).attach();

    this.init();
  }

  /**
   * Build iframe wrapper
   */
  init() {
    const { classes } = this.styleObj;

    this.body.innerHTML = `
      <div class="${classes.mfBg}">
         <div class="${classes.mfWrapper}" style="width: ${this.width}; height: ${this.height}">
            <iframe src="${this.url}" class="${classes.mfIframe}"></iframe>
         </div>   
      </div>
    `;
  }

  /**
   * Close popup (usage: window.mfPopup.closePopup())
   */
  closePopup() {
    const { classes } = this.styleObj;
    const bg = document.getElementsByClassName(classes.mfBg)[0];
    bg.style.opacity = 0;

    if (typeof this.closeCallback === 'function') this.closeCallback();
    setTimeout(() => {
      bg.remove();
    }, 2000);
  }

  /**
   * Show popup (usage: window.mfPopup.showPopup(1000))
   * @param time
   */
  showPopup(time = 3000) {
    const { classes } = this.styleObj;
    const bg = document.getElementsByClassName(classes.mfBg)[0];
    setTimeout(() => {
      bg.style.opacity = 1;
    }, time);
  }
}