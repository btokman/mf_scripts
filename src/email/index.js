import Popup from '../popup';
import Cookie from "../cookie";
import { EMAIL_DENY_TIMEOUT, WEBPUSH_CLOSE_TIMEOUT } from "../session/config";

/**
 * Init subscribe email
 */
export default class Email {
  popup = null;

  init() {
    window.mfPopup = this.initPopup();
  }

  /**
   * Apply popup to session object
   * @returns {Popup}
   */
  initPopup() {
    const { url, width, height } = window.mfSession.settings.subscribe;

    if (!url || !width || !height) return;

    this.popup = new Popup(url, width, height);
    this.popup.closeCallback = this.popupCloseCallback;

    return this.popup;
  }

  /**
   * @returns {WebPushFcm}
   */
  popupCloseCallback() {
    Cookie.set('emailDeny', true, { expires: EMAIL_DENY_TIMEOUT });

    return this;
  }
}