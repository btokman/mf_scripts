/**
 * Firebase script url
 * @type {string}
 */
export const FIREBASE_JS = 'https://www.gstatic.com/firebasejs/4.5.0/firebase-app.js';
/**
 * Firebae messaging script url
 * @type {string}
 */
export const FIREBASE_MESSAGING = 'https://www.gstatic.com/firebasejs/4.5.0/firebase-messaging.js';

/**
 * Mailfire api url
 * @type {string}
 */
export const API_URL = 'https://n.mailfire.io/';


/**
 * Load script to dom
 * @param {string} url
 */
export function loadScript(url) {
  return new Promise(resolve => {
    let l = document.createElement('script');
    l.src = url;
    l.type = 'text/javascript';
    l.async = true;
    l.onload = resolve;
    let s = document.getElementsByTagName('body')[0];
    s.parentNode.insertBefore(l, s);
  });
}

/**
 * Check if firebase token was send to server
 * @param token
 * @returns {boolean}
 */
export function isTokenSentToServer(token) {
  return window.localStorage.getItem('sentFirebaseMessagingToken') === token;
}

/**
 * Set flag that token was send
 * @param token
 */
export function setTokenWasSent(token) {
  window.localStorage.setItem('sentFirebaseMessagingToken', token || '');
}

/**
 * Jsonp request
 * @param uri
 * @returns {Promise<any>}
 */
export function jsonp(uri) {
  return new Promise((resolve, reject) => {
    const id = `_${Math.round(10000 * Math.random())}`;
    const callbackName = `jsonp_callback_${id}`;

    window[callbackName] = function (data) {
      delete window[callbackName];
      const ele = document.getElementById(id);
      ele.parentNode.removeChild(ele);
      resolve(data);
    };

    const src = `${uri}&callback=${callbackName}`;
    const script = document.createElement('script');
    script.src = src;
    script.id = id;
    script.addEventListener('error', reject);
    (document.getElementsByTagName('head')[0] || document.body || document.documentElement).appendChild(script);
  });
}