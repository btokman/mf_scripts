import {
  FIREBASE_JS,
  FIREBASE_MESSAGING,
  isTokenSentToServer,
  jsonp,
  loadScript,
  setTokenWasSent,
} from '../helpers/_helpers';
import WebPushTracker from '../helpers/_webpushTracker';
import Popup from '../popup';
import Session from '../session';
import Cookie from '../cookie';
import { WEBPUSH_DENY_TIMEOUT } from '../session/config';

export default class WebPushFcm {
  timer = null;

  popup = null;

  constructor() {
    window.mfPopup = this.initPopup();
  }

  async init() {
    return this.initFireBase();
  }

  auto() {
    this.init();

    this.timer = setTimeout(window.mfPopup.showPopup, 3000);
  }

  /**
   * Init popup
   * @returns { Popup }
   */
  initPopup() {
    const { url, width, height } = window.mfSession.settings.webpush;

    if (!url || !width || !height) return;

    WebPushTracker.trackAttempt({ popup_type: 1 });

    this.popup = new Popup(url, width, height);
    this.popup.closeCallback = this.popupCloseCallback;
    this.popup.subscribeEvent = () => this.subscribe();

    return this.popup;
  }

  /**
   * @returns {WebPushFcm}
   */
  popupCloseCallback() {
    WebPushTracker.trackDeny({ popup_type: 2 });
    Cookie.set('pushDeny', true, { expires: WEBPUSH_DENY_TIMEOUT });

    return this;
  }

  /**
   * Init firebase scripts
   * @returns {Promise<void>}
   */
  async initFireBase() {
    if (!('serviceWorker' in navigator)) {
      return;
    }
    /** Wait while scripts downloading */
    await loadScript(FIREBASE_JS);

    await loadScript(FIREBASE_MESSAGING);

    /** init firabase */
    const req = await navigator.serviceWorker.register(window.mfSession.sw);

    firebase.initializeApp({
      messagingSenderId: window.mfSession.msgSenderId,
    });

    window.mfSession.messaging = firebase.messaging();
    window.mfSession.messaging.useServiceWorker(req);
  }

  /**
   * Subscribe user
   * @returns {Promise<void>}
   */
  async subscribe() {
    /** Wait while user data would be set */
    if (!window.mfSession.userId) {
      await window.mfSession.getUserInfo();
    }
    /** Track that popup was showed */
    if (Notification.permission === 'default') {
      WebPushTracker.trackAttempt({ userId: Session.userId, popup_type: 2 });
    }

    await this.subscribeUser();
  }

  /**
   * Subscribe user (see firabase docks)
   * @returns {Promise<void>}
   */
  async subscribeUser() {
    await Notification.requestPermission();

    await navigator.serviceWorker.ready;

    await window.mfSession.messaging.requestPermission();

    const token = await window.mfSession.messaging.getToken();

    token ? this.sendToken(token) : setTokenWasSent(false);
  }

  /**
   * Send user token to server
   * @param token
   */
  sendToken(token) {
    if (isTokenSentToServer(token)) return;

    const data = {
      fcm_meta: { token },
    };

    if (window.mfSession.userId) {
      data.userId = +window.mfSession.userId;
    } else {
      data.hash = window.mfSession.guestId;
    }

    const request = new XMLHttpRequest();
    const url = `https://api.mailfire.io/v1/webpush/project/${window.mfSession.project}`;

    request.open('POST', url, true);

    request.onload = function () {
      const response = JSON.parse(this.responseText);
      const pushUserId = response.data.push_user_id;

      if (!pushUserId) return;

      Cookie.set('push_user_id', pushUserId);

      const date = new Date();
      date.setDate(date.getDate() + 365);

      jsonp('https://n.mailfire.io/users/setcookie');

      const cookieRequest = new XMLHttpRequest();

      cookieRequest.open('POST', 'https://n.mailfire.io/users/setcookie', true);

      const userData = {
        push_user_id: {
          value: pushUserId,
          expire: date.toUTCString(),
        },
      };

      userData.append('push_user_id[value]', pushUserId);
      userData.append('push_user_id[expire]', date.toUTCString());

      cookieRequest.send(userData);
    };

    request.send(JSON.stringify(data));
    setTokenWasSent(token);
  }
}